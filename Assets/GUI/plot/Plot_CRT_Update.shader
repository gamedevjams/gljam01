﻿Shader "tg/Pot CRT Update"
{
	Properties
	{
		//_Values01 ( "Values01", float[] ) = 0
		_Iteration ( "Iteration", Int ) = 0
		_ValuesCount ( "Values Count", Int ) = 0
		_FGColor ( "Foreground", Color ) = ( 0,0,0.5,1 )
		_BGColor ( "Background", Color ) = ( 0,0,0,0 )
	}
		SubShader
	{
		// No culling or depth
		//Cull Off ZWrite Off ZTest Always
		Lighting Off
		Blend One Zero

		Pass
		{
			CGPROGRAM
			// Upgrade NOTE: excluded shader from DX11, OpenGL ES 2.0 because it uses unsized arrays
			//#pragma exclude_renderers d3d11 gles

			#include "UnityCustomRenderTexture.cginc"
			#pragma vertex CustomRenderTextureVertexShader
			#pragma fragment frag
			#pragma target 3.0

			//#include "UnityCG.cginc"

			float4 _FGColor;
			float4 _BGColor;
			uniform float _Values01[128];
			int    _Iteration;
			int    _ValuesCount;

			float4 frag( v2f_customrendertexture IN ) : COLOR
			{
				float4 old = tex2D( _SelfTexture2D, IN.globalTexcoord.xy );
				int px_x = (int)( IN.globalTexcoord.x * _CustomRenderTextureWidth );
				for ( int i = 0; i != (int)_Values01.Length && i != _ValuesCount; ++i )
				{
					int rel_iter = ( _Iteration + i ) % _CustomRenderTextureWidth;
					if ( px_x == rel_iter && _Values01[i] >= 0 )
					{
						return IN.globalTexcoord.y > _Values01[i] ? _BGColor : _FGColor;
					}
				}
				{
					int rel_iter = ( _Iteration + _ValuesCount ) % _CustomRenderTextureWidth;
					if ( px_x == rel_iter )
					{
						float4 inv = _FGColor;
						inv.rgb = float3(1,1,1) - inv.rgb;
						return inv;
					}
				}
				return old;
			}
			ENDCG
		}
	}
}