﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace tg
{
	[ExecuteAlways]
	public class Tile_Map_Sprite : MonoBehaviour
	{
		public Renderer the_renderer;
		public RawImage raw_image;
		public int2   tile_xy;
		public int2   tiles_grid;
		public float2 cell_margin_in_cell_pc_bl;
		public float2 cell_margin_in_cell_pc_tr;

		public MaterialPropertyBlock mpb;

		public void change_sprite( int2 new_tile_xy )
		{
			if ( all( current_tile_xy == new_tile_xy ) )
			{
				return;
			}

			//tg.log( $"Tiled Map Sprite: {{ {current_tile_xy.x}, {current_tile_xy.y} }} => {{ {new_tile_xy.x}, {new_tile_xy.y} }}" );

			this.tile_xy = new_tile_xy;

			tiles_grid = max( 0, tiles_grid );
			tile_xy = clamp( tile_xy, int2(0,0), tiles_grid - 1 );

			tiling = 1f / float2( tiles_grid );

			cell_margin_in_uv_bl = cell_margin_in_cell_pc_bl * tiling;
			cell_margin_in_uv_tr = cell_margin_in_cell_pc_tr * tiling;

			offset = tile_xy * tiling + cell_margin_in_uv_bl;
			tiling -= cell_margin_in_uv_tr;

			if ( the_renderer != null )
			{
				#if UNITY_EDITOR
				if ( mpb == null )
				{
					mpb = new MaterialPropertyBlock();
				}
				#endif
				update_mpb();
				//tg.log( $"Tiled Map Sprite: {{ {current_tile_xy.x}, {current_tile_xy.y} }} => {{ {new_tile_xy.x}, {new_tile_xy.y} }}" );
			}
			else if(raw_image != null)
			{
				raw_image.uvRect = new Rect(offset.x, offset.y, tiling.x, tiling.y);
			}

			#if UNITY_EDITOR
			if ( EditorApplication.isPlaying )
			{
				current_tile_xy = tile_xy;
			}
			#else
			current_tile_xy = tile_xy;
			#endif
		}
		public
		void
		update_mpb()
		{
			mpb.SetVector( main_texture_id, new Vector4( tiling.x, tiling.y, offset.x, offset.y ) );
			the_renderer.SetPropertyBlock( mpb );
		}

		#region private
		private int    main_texture_id;
		private float2 offset;
		private float2 tiling;
		private float2 cell_margin_in_uv_bl;
		private float2 cell_margin_in_uv_tr;

		private int2 current_tile_xy = int2(-1,-1);

		[Button]
		private void ApplyTile()
		{
			change_sprite( tile_xy );
		}
		#endregion

		#region Unity
		private void Awake()
		{
			main_texture_id = Shader.PropertyToID( "_MainTex_ST" );
			mpb = new MaterialPropertyBlock();
		}
		private void Start()
		{
			change_sprite( tile_xy );
		}
		#endregion
	}
}
