﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using tg;
using Unity.Mathematics;
using UnityEngine;
using static Unity.Mathematics.math;

public class Picker : MonoBehaviour
{
	public Entity entity;

	public
	void
	enable()
	{
		gameObject.SetActive( true );
	}
	public
	void
	disable()
	{
		gameObject.SetActive( false );
	}

	public
	Weapon
	PickWeapon()
	{
		if ( pickableWeapons.Count == 0 )
			return null;

		Weapon retWeapon = null;

		if ( pickableWeapons.Count == 1 )
		{
			retWeapon = pickableWeapons.First().Key;
			pickableWeapons.Clear();
			return retWeapon;
		}

		float min_distance = float.MaxValue;
		float3 entityPos = entity.pos;

		float3 pos;
		float dist;

		foreach ( var elem in pickableWeapons )
		{
			pos = elem.Value.position;
			dist = distance( entityPos, pos );

			if ( dist < min_distance )
			{
				min_distance = dist;
				retWeapon = elem.Key;
			}
		}

		if ( retWeapon != null )
		{
			pickableWeapons.Remove( retWeapon );
		}

		return retWeapon;
	}

	#region private
	private Collider the_collider;
	private Dictionary<Weapon, Transform> pickableWeapons = new Dictionary<Weapon, Transform>();
	#endregion

	#region Unity
	private void Awake()
	{
		the_collider = GetComponent<Collider>();

		if ( entity == null )
			entity = GetComponentInParent<Entity>();
	}
	private void Start()
	{
		if ( entity == null )
			disable();
	}
	private void Update()
	{
	}
	private void OnTriggerEnter( Collider other )
	{
		Weapon weapon = other.GetComponentInParent<Weapon>();

		if ( weapon != null && weapon.is_a_drop )
		{
			if ( ! pickableWeapons.ContainsKey( weapon ) )
			{
				pickableWeapons.Add( weapon, other.GetComponentInParent<Transform>() );
			}
		}
	}
	private void OnTriggerStay( Collider other )
	{
		Weapon weapon = other.GetComponentInParent<Weapon>();

		if ( weapon != null && !weapon.is_a_drop )
		{
			if ( pickableWeapons.ContainsKey( weapon ) )
			{
				pickableWeapons.Remove( weapon );
			}
		}
	}
	private void OnTriggerExit( Collider other )
	{
		Weapon weapon = other.GetComponentInParent<Weapon>();

		if ( weapon != null && pickableWeapons.ContainsKey( weapon ) )
		{
			pickableWeapons.Remove( weapon );
		}
	}
	#endregion
}
