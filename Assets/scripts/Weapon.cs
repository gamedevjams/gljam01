﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System;

namespace tg
{
	public class Weapon : MonoBehaviour
	{
		public Entity owner;
		public Transform weapon_holder;
		public bool is_a_drop => owner == null;
		public Rigidbody rb;
		public Animator animator;

		[Header("Melee")]
		public bool has_melee = true;
		public List<Entity_Hitbox> hitboxes = new List<Entity_Hitbox>();
		public List<Collider> physics_colliders = new List<Collider>();
		public float min_range = 0f;
		public float max_range = 0.5f;

		[Header("Cast")]
		public bool has_cast;
		public Transform cast_point;
		public float cast_damage;
		public float cast_range_max;

		public bool cast_is_heal => cast_damage < 0;

		[Button]
		public
		void
		cast()
		{
			animator.SetTrigger( cast_anim_id );
		}
		[Button]
		public
		void
		abort_cast()
		{
			animator.SetTrigger( abort_cast_anim_id );
		}
		public
		void
		cast_now()
		{
			// TODO(theGiallo): IMPLEMENT
		}
		[Button]
		public
		void
		attack()
		{
			animator.SetTrigger( attack_anim_id );
			for ( int i = 0; i != hitboxes.Count; ++ i )
			{
				hitboxes[i].enable();
			}
		}
		[Button]
		public
		void
		stop_attack()
		{
			animator.SetTrigger( abort_attack_anim_id );
			attack_stopped();
		}
		public
		void
		attack_stopped()
		{
			log( "attack_stopped" );
			for ( int i = 0; i != hitboxes.Count; ++ i )
			{
				hitboxes[i].disable();
			}
		}
		[Button]
		public
		void
		drop()
		{
			rb.isKinematic = false;
			rb.useGravity = true;
			transform.SetParent( null );
			if ( animator != null ) animator.enabled = false;
			for ( int i = 0; i != physics_colliders.Count; ++ i )
			{
				physics_colliders[i].enabled = true;
				physics_colliders[i].isTrigger = false;
			}
			for ( int i = 0; i != hitboxes.Count; ++ i )
			{
				hitboxes[i].disable();
				hitboxes[i].owner = null;
			}
			owner = null;
		}
		public
		void
		pick_up( Entity new_owner, Transform weapon_holder )
		{
			owner = new_owner;
			rb.isKinematic = true;
			rb.useGravity = false;
			transform.SetParent( weapon_holder );
			if ( animator != null ) animator.enabled = true;
			for ( int i = 0; i != physics_colliders.Count; ++ i )
			{
				physics_colliders[i].enabled = false;
			}
			for ( int i = 0; i != hitboxes.Count; ++ i )
			{
				hitboxes[i].disable();
				hitboxes[i].the_collider.isTrigger = true;
				hitboxes[i].owner = owner;
			}
		}

		#region private
		private int attack_anim_id;
		private int cast_anim_id;
		private int abort_cast_anim_id;
		private int abort_attack_anim_id;
		#endregion

		#region Unity
		private void Awake()
		{
			attack_anim_id       = Animator.StringToHash( "Attack" );
			cast_anim_id         = Animator.StringToHash( "Cast" );
			abort_cast_anim_id   = Animator.StringToHash( "Abort Cast" );
			abort_attack_anim_id = Animator.StringToHash( "Abort Attack" );
			if ( owner != null && weapon_holder != null )
			{
				pick_up( owner, weapon_holder );
			} else
			{
				drop();
			}

			AnimationEvent ae = new	AnimationEvent();
			ae.functionName = "attack_stopped";
			if ( animator != null )
			{
				var clips = animator.runtimeAnimatorController.animationClips;
				for ( int i = 0; i != clips.Length; ++i )
				{
					var clip = animator.runtimeAnimatorController.animationClips[i];
					log( $"clip.name: {clip.name}" );
					if ( clip.name.ToLower().Contains( "attack" ) )
					{
						ae.time = clip.averageDuration;
						if ( ! Array.Exists( clip.events, e=>e.functionName == ae.functionName ) )
						{
							clip.AddEvent( ae );
						}
					}
				}
			}
		}
		private void Start()
		{
		}
		private void Update()
		{
		}
		#endregion
	}
}
