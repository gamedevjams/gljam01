﻿using EasyButtons;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
#if UNITY_EDITOR
using UnityEngine;
#endif

namespace tg
{
	using static tg;

	//using ColoredMaterials = ArrayWrapper<ColorChanger.ColoredMaterial>;
	[Serializable]
	public class ColoredMaterials : ArrayWrapper<ColorChanger.ColoredMaterial>
	{
		public ColoredMaterials( int length ) : base( length ) { }
	}

	public
	class
	ColorChanger : MonoBehaviour
	{
		[Serializable]
		public
		class
		ColoredMaterial
		{
			public
			enum
			Type
			{
				SHIRT,
				SKIN,
				PANTS,
				BELT,
				FACE,
				HAIR,
			}
			public Type type;
			public Color32 color;
			[NonSerialized]
			public Material original_material;
			[NonSerialized]
			public Material changed_material;
		}
		public Renderer target_renderer;
		public ColoredMaterials colored_materials;

		#region private
		private int color_id;

		[Button]
		private
		void
		init()
		{
			color_id = Shader.PropertyToID( "_Color" );

			Material[] materials = target_renderer.materials;
			colored_materials = new ColoredMaterials( materials.Length );

			for ( int i = 0; i != materials.Length; ++i )
			{
				ColoredMaterial cm = colored_materials[i] = new ColoredMaterial();
				cm.type = (ColoredMaterial.Type)i;
				cm.original_material = cm.changed_material = new Material( materials[i] );
				cm.changed_material.name = $"Changed{cm.type}Material";
				materials[i] = cm.changed_material;
			}

			target_renderer.materials = materials;
		}
		[Button]
		private
		void
		update_color()
		{

			for ( int i = 0; i != colored_materials.Length; ++i )
			{
				ColoredMaterial cm = colored_materials[i];
				cm.changed_material.SetColor( color_id, cm.color );
			}
			//log( $"renderer: {renderer.materials[SKIN_MATERIAL_INDEX].name}" );
			//log( $"changed: {changed_material_skin.name}" );
		}
		#endregion private

		#region Unity
		void
		Start()
		{
			if ( colored_materials == null )
			{
				init();
			}
			update_color();

		}
		#if UNITY_EDITOR
		private void OnValidate()
		{
			if ( colored_materials == null )
			{
				init();
			}
			if ( colored_materials[0] != null
			  && colored_materials[0].changed_material != null )
			{
				update_color();
			}
		}
		#endif
		#endregion Unity
	}
}
