﻿using System;
using Unity.Mathematics;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using static Unity.Mathematics.math;

namespace tg
{
	[ExecuteAlways]
	public class Impostor_Four_Dirs : MonoBehaviour
	{
		public Tile_Map_Sprite map_sprite;
		public Sprite_Animator sprite_animator;
		public Transform character_transform;
		public Transform root_transform;
		public bool control_billboard_rotation;
		public int2 starting_tile_xy;

		#region private
		private int2 current_tile_xy;
		#endregion

		#region Unity
		private void Awake()
		{
			if (map_sprite != null)
			{
				starting_tile_xy = map_sprite.tile_xy;
				current_tile_xy = starting_tile_xy;
			}

			if ( root_transform == null )
			{
				root_transform = transform;
			}

			#if false
			Camera_Render_Events.Subscribe( 32, change_sprite, Camera_Render_Events.Event.OnPreRender );
			#endif
		}

		#if UNITY_EDITOR
		private void OnValidate()
		{
			if ( ! EditorApplication.isPlaying
			  && ! EditorApplication.isPlayingOrWillChangePlaymode
			  )
			{
				if ( map_sprite != null )
				{
					current_tile_xy = starting_tile_xy;
					map_sprite.change_sprite( current_tile_xy );
				}

				if ( sprite_animator != null )
				{
					sprite_animator.change_character_tile();
				}
			}
		}
		#endif
		#if true
		private void OnWillRenderObject()
		{
			if ( ! gameObject.activeInHierarchy || ! enabled )
			{
				return;
			}
			change_sprite( Camera.current );
		}
		#endif
		private void change_sprite( Camera the_camera )
		{
			if ( map_sprite == null || character_transform == null )
			{
				return;
			}

			float3 camera_pos   = the_camera.transform.position;
			float3 obj_pos      = character_transform.position;
			float3 obj_fw       = character_transform.forward;
			float2 cameraPos    = float2( camera_pos.x, camera_pos.z );
			float2 characterPos = float2( obj_pos.x, obj_pos.z );
			float2 characterFw  = float2( obj_fw.x, obj_fw.z );

			float vector2DAngle = Vector2.SignedAngle( characterFw, cameraPos - characterPos );

			//tg.log($"Current Angle: {vector2DAngle}");

			int2 new_tile;

			if ( sprite_animator == null )
				new_tile.x = starting_tile_xy.x;
			else
				new_tile.x = sprite_animator.current_sprite_animation_x;

			float3 bb_dir = default;
			if ( vector2DAngle > -45 && vector2DAngle <= 45 )
			{
				new_tile.y = starting_tile_xy.y;
				bb_dir = obj_fw;
			} else
			if ( vector2DAngle > 45 && vector2DAngle <= 135 )
			{
				new_tile.y = starting_tile_xy.y - 2;
				bb_dir = -character_transform.right;
			} else
			if ( vector2DAngle > -135 && vector2DAngle <= -45 )
			{
				new_tile.y = starting_tile_xy.y - 3;
				bb_dir = character_transform.right;
			} else
			{
				new_tile.y = starting_tile_xy.y - 1;
				bb_dir = -obj_fw;
			}

			if ( control_billboard_rotation )
			{
				root_transform.rotation = Quaternion.LookRotation( bb_dir, character_transform.up );
			}

			//if ( any( new_tile != current_tile_xy ) )
			{
				current_tile_xy = new_tile;
				map_sprite.change_sprite( new_tile );
			}

			//tg.log( $"Current Tile: {{ {current_tile_xy.x}, {current_tile_xy.y} }}" );
		}
		#endregion
	}
}