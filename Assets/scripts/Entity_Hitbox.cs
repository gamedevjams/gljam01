﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	[RequireComponent(typeof(Collider))]
	public class Entity_Hitbox : MonoBehaviour
	{
		public Entity owner;
		public float  damage;
		public Collider the_collider;

		public
		void
		enable()
		{
			gameObject.SetActive( true );
		}
		public
		void
		disable()
		{
			gameObject.SetActive( false );
		}

		#region private
		#endregion

		#region Unity
		private void Awake()
		{
			the_collider = GetComponent<Collider>();
		}
		private void Start()
		{
		}
		private void Update()
		{
		}
		private void OnTriggerEnter( Collider other )
		{
			Entity_Hurtbox entity_hb = other.GetComponent<Entity_Hurtbox>();
			if ( entity_hb != null && entity_hb.entity != null )
			{
				Entity entity = entity_hb.entity;
				if ( owner != null )
				{
					if ( entity != owner )
					{
						//int hb_index = hitboxes.FindIndex( _hb => _hb.collider == other );
						//if ( hb_index >= 0 )
						{
							float current_hp = entity.status.hp;
							float new_hp = entity.status.hp.change( current_hp - damage, owner );
						}
					}
				} else
				{
					// TODO(theGiallo): here entity can pickup the weapon
				}
			}
		}
		#endregion
	}
}
