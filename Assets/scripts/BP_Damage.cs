﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System;

namespace tg
{
	public class BP_Damage : Billboard_Particle
	{
		override protected GameObject the_prefab { get => _the_prefab_go; set { _the_prefab_go = value; } }
		override protected Pool<Billboard_Particle> the_pool { get => _the_pool; set { _the_pool = value; } }
		private static Pool<Billboard_Particle> _the_pool;
		private static BP_Damage _the_prefab;
		private static GameObject _the_prefab_go;

		public Animator animator;
		public int2 full_heart;
		public int2 half_heart;

		public Color color;

		public static
		Billboard_Particle
		spawn( float3 position, int heart_halves )
		{
			Billboard_Particle ret = _the_pool.take_away();
			ret.transform.position = position;
			ret.gameObject.SetActive( true );
			if ( heart_halves == 1 )
			{
				ret.tile_map_sprite.change_sprite( _the_prefab.half_heart );
			} else
			{
				ret.tile_map_sprite.change_sprite( _the_prefab.full_heart );
			}
			ret.transform.rotation = Quaternion.AngleAxis( 360 * UnityEngine.Random.value, Vector3.up );
			return ret;
		}
		public
		void
		animation_end()
		{
			put_back_in_pool();
		}

		#region private
		private int anim_id;
		private string animation_name = "the_animation";
		private int    color_id;

		private
		void
		play()
		{
			log( $"play {name}" );
			animator.Play( anim_id );
		}
		#endregion

		#region Unity
		private void Awake()
		{
			if ( the_pool == null )
			{
				gameObject.SetActive( false );
				the_pool = new Pool<Billboard_Particle>();
				the_prefab = gameObject;
				_the_prefab = this;
				the_pool.init( this );
			}
			anim_id = Animator.StringToHash( animation_name );

			color_id = Shader.PropertyToID( "_Color" );

			AnimationEvent ae = new	AnimationEvent();
			ae.functionName = "animation_end";
			var clips = animator.runtimeAnimatorController.animationClips;
			for ( int i = 0; i != clips.Length; ++i )
			{
				var clip = animator.runtimeAnimatorController.animationClips[i];
				log( $"clip.name: {clip.name}" );
				if ( clip.name == animation_name )
				{
					ae.time = clip.averageDuration;
					if ( ! Array.Exists( clip.events, e=>e.functionName == ae.functionName ) )
					{
						clip.AddEvent( ae );
					}
				}
			}
		}
		private void Start()
		{
			mpb = new MaterialPropertyBlock();
		}
		private void OnEnable()
		{
			play();
		}
		private void OnDisable()
		{
		}
		private MaterialPropertyBlock mpb;
		private void Update()
		{
			tile_map_sprite.mpb.SetColor( color_id, color );
			tile_map_sprite.update_mpb();
			//tile_map_sprite.the_renderer.SetPropertyBlock( mpb );
			//tile_map_sprite.change_sprite( tile_map_sprite.tile_xy );
			//tile_map_sprite.the_renderer.SetPropertyBlock( tile_map_sprite.mpb );
		}
		private void OnDestroy()
		{
			if ( this == _the_prefab )
			{
				the_pool = null;
				_the_prefab = null;
			}
		}
		#endregion
	}
}
