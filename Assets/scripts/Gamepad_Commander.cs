﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using static tg.Character_Controller;
using System.IO;
using System.Runtime.Remoting.Messaging;

namespace tg
{
	public class Gamepad_Commander : MonoBehaviour
	{
		public Character_Controller controlled_character;
		public Game_Input.Gamepad_Status gamepad;
		public int gamepad_device_id = -1;

		#region private
		private
		int
		action_to_button( Action_Index a )
		{
		#if UNITY_IS_NOT_A_PIECE_OF_SHIT
			int ret =
			a switch
			{
				Action_Index.INTERACT => (int)Game_Input.Gamepad_Button.R1,
				Action_Index.RUN      => (int)Game_Input.Gamepad_Button.L1,
				Action_Index.USE      => (int)Game_Input.Gamepad_Button.R2,
				Action_Index.USE_ALT  => (int)Game_Input.Gamepad_Button.L2,
				Action_Index.JUMP     => (int)Game_Input.Gamepad_Button.A,
				Action_Index.ROLL     => (int)Game_Input.Gamepad_Button.B,
				Action_Index.__COUNT  => -1,
			};
		#else
			int ret = -1;
			switch ( a )
			{
				case Action_Index.INTERACT: ret = (int)Game_Input.Gamepad_Button.R1; break;
				case Action_Index.RUN     : ret = (int)Game_Input.Gamepad_Button.L1; break;
				case Action_Index.USE     : ret = (int)Game_Input.Gamepad_Button.R2; break;
				case Action_Index.USE_ALT : ret = (int)Game_Input.Gamepad_Button.L2; break;
				case Action_Index.JUMP    : ret = (int)Game_Input.Gamepad_Button.A; break;
				case Action_Index.ROLL    : ret = (int)Game_Input.Gamepad_Button.B; break;
				case Action_Index.__COUNT : ret = -1; break;
			};
		#endif
			return ret;
		}

		private
		void
		update_control_commands( Controls_Commands controls_commands )
		{
			for ( int i = 0; i != ACTIONS_COUNT; ++i )
			{
				controls_commands.actions_on      [i] = action_on(       (Action_Index)i ) ? 1 : 0;
				controls_commands.actions_just_on [i] = action_just_on(  (Action_Index)i ) ? 1 : 0;
				controls_commands.actions_just_off[i] = action_just_off( (Action_Index)i ) ? 1 : 0;
			}
			controls_commands.movement_v2 = movement_direction();
		}
		private
		bool
		action_just_on( Action_Index a )
		{
			bool ret = gamepad.buttons_just_activated[action_to_button( a )] == 1;
			return ret;
		}
		private
		bool
		action_just_off( Action_Index a )
		{
			bool ret = gamepad.buttons_just_deactivated[action_to_button( a )] == 1;
			return ret;
		}
		private
		bool
		action_on( Action_Index a )
		{
			bool ret = gamepad.buttons_active[action_to_button( a )] == 1;
			return ret;
		}
		private
		float2
		movement_direction()
		{
			float2 ret = gamepad.sticks_value[0];
			return ret;
		}

		[Button]
		private
		void
		assign_gamepad()
		{
			tg.debug_assert( gamepad_device_id >= 0 );
			gamepad = Game_Input.instance.gamepads[gamepad_device_id];
		}
		#endregion

		#region Unity
		private void Awake()
		{
		}
		private void Start()
		{
			assign_gamepad();
		}
		private void FixedUpdate()
		{
			if ( controlled_character != null )
			{
				update_control_commands( controlled_character.controls_commands );
			}
		}
		#endregion
	}
}
