﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System;

namespace tg
{
	public class Entity : MonoBehaviour
	{
		public enum
		Type
		{
			ADVENTURER,
			SKELETON,
			GOLEM,
			MAGE,
			DARK_KNIGHT,
			GHOST,
			ZOMBIE,
			KNIGHT,
			INSECTOID,
			ROCK,
			GRASS,
			TREE,
			MUSHROOM,
			TRAPDOOR,
			STAIRS,
			__COUNT,
		}
		public class
		StatusEntry<T>// where T : struct, IComparable<T>, IEqualityComparer<T>, IEquatable<T>, IStructuralEquatable
		{
			public class
			Callback_Data
			{
				public T      previous;
				public T      new_value;
				public Entity entity_source;
				public bool   recurse;
			}
			public Event_With_Priority_Interruptable<Callback_Data>.ISubscribable on_will_change => _on_will_change;
			public Event_With_Priority_Interruptable<Callback_Data>.ISubscribable on_did_change  => _on_did_change;

			public
			T
			change( T new_value, Entity entity_source )
			{
				Callback_Data cd = new Callback_Data()
				{
					previous      = v,
					new_value     = new_value,
					entity_source = entity_source,
				};

				_on_will_change.Invoke( cd );
				if ( cd.recurse )
				{
					change( cd.new_value, cd.entity_source );
				} else
				{
					v = cd.new_value;
				}
				_on_did_change.Invoke( cd );

				//for ( var en = _on_will_change.InvokeEn( cd ); en.MoveNext(); )
				//{
				//	// --- compute delta
				//	cd.proposed = new_value;
				//}

				return cd.new_value;
			}
			public static implicit operator T( StatusEntry<T> se )
			{
				return se.v;
			}
			public T value => v;

			#region private
			private T v;
			private Event_With_Priority_Interruptable<Callback_Data> _on_will_change = new Event_With_Priority_Interruptable<Callback_Data>();
			private Event_With_Priority_Interruptable<Callback_Data> _on_did_change  = new Event_With_Priority_Interruptable<Callback_Data>();
			#endregion private
		}
		public class Status
		{
			public StatusEntry<bool > has_life         = new StatusEntry<bool>();
			public StatusEntry<bool > is_alive         = new StatusEntry<bool>();
			public StatusEntry<float> hp               = new StatusEntry<float>();
			public StatusEntry<float> max_hp           = new StatusEntry<float>();
			public StatusEntry<float> normal_max_hp    = new StatusEntry<float>();
			public StatusEntry<float> dps              = new StatusEntry<float>();
			public StatusEntry<float> fov_deg          = new StatusEntry<float>();
			public StatusEntry<float> attack_min_range = new StatusEntry<float>();
			public StatusEntry<float> attack_max_range = new StatusEntry<float>();
			public StatusEntry<float> cast_min_range   = new StatusEntry<float>();
			public StatusEntry<float> cast_max_range   = new StatusEntry<float>();
			public StatusEntry<Weapon> weapon          = new StatusEntry<Weapon>();
		}

		public static List<Entity> all_entities = new List<Entity>();
		public static List<Entity>[] entities_by_type = new List<Entity>[(int)Type.__COUNT];
		static Entity()
		{
			for ( int i = 0; i != entities_by_type.Length; ++i )
			{
				entities_by_type[i] = new List<Entity>();
			}
		}

		public static
		void
		clean_static_stuff()
		{
			all_entities.Clear();
			for ( int i = 0; i != entities_by_type.Length; ++i )
			{
				entities_by_type[i].Clear();
			}
			next_uuid = 0;
		}

		private static
		void
		register( Entity e )
		{
			e.uuid = next_uuid++;
			all_entities.Add( e );
			entities_by_type[(int)e.type].Add( e );
		}
		private static
		void
		unregister( Entity e )
		{
			all_entities.Remove( e );
			entities_by_type[(int)e.type].Remove( e );
		}

		public int    uuid   { get; private set; }
		[SerializeField]
		private Type  _type;
		public Type type => _type;
		public Status status { get; private set; } = new Status();

		public float normal_max_hp = 10;
		public float dps           = 1;
		public float fov_deg       = 120;
		public bool  has_life      = true;

		public float3     pos => transform.position;
		public float3     fw  => transform.forward;
		public Quaternion rot => transform.rotation;

		public float radius = 0.5f;
		public Collider entity_collider;
		public Transform right_hand;
		public Character_Controller controller;
		public Sprite_Animator      animator;
		public Picker picker;

		public float unscaled_height;
		public float height => unscaled_height * transform.localScale.y;

		public
		float
		distance( Entity other )
		{
			float3 other_pos    = other.transform.position;
			float  other_radius = other.radius;
			float  ret = distance( other_pos, other_radius );
			return ret;
		}
		public
		float
		distance( float3 other_pos, float other_radius )
		{
			float3 pos = transform.position;
			float  ret = length( other_pos - pos ) - radius - other_radius;
			return ret;
		}
		public
		bool
		is_in_fov( Entity other )
		{
			bool ret = false;
			float3 other_pos    = other.transform.position;
			float  other_radius = other.radius;
			ret = is_in_fov( other_pos, other_radius );
			return ret;
		}
		public
		bool
		is_in_fov( float3 other_pos, float other_radius )
		{
			bool ret = false;

			float3 pos = transform.position;
			float3 fw = transform.forward;

			float2 pos2       = float2( pos.x, pos.z );
			float2 other_pos2 = float2( other_pos.x, other_pos.z );
			float2 fw2        = float2( fw.x, fw.z );
			float2 to_other2  = other_pos2 - pos2;
			float  fov_deg    = status.fov_deg;
			float  hfov_deg   = fov_deg * 0.5f;

			// NOTE(theGiallo): it's clockwise
			float fw_angle = Vector2.SignedAngle( fw2, to_other2 );
			if ( abs( fw_angle ) <= hfov_deg )
			{
				ret = true;
			} else
			{
				float2 right = normalize( float2( to_other2.y, -to_other2.x ) );
				float2 side2 = right * sign( fw_angle );
				float2 border2 = other_pos2 + side2 * other_radius;
				float2 to_border2 = border2 - pos2;
				fw_angle = Vector2.SignedAngle( fw2, to_border2 );
				ret = abs( fw_angle ) <= hfov_deg;
			}
			return ret;
		}

		public
		void
		use()
		{
			if ( status.weapon.value != null )
			{
				status.weapon.value.attack();
			}
		}
		public
		void
		use_alt()
		{
			if ( status.weapon.value != null )
			{
				status.weapon.value.cast();
			}
		}
		public
		void
		interact()
		{
			if ( picker != null )
			{
				Weapon weapon = picker.PickWeapon();
				if ( weapon != null && right_hand != null )
				{
					if ( status.weapon.value != null )
					{
						status.weapon.value.drop();
						status.weapon.change( null, this );
					}

					status.weapon.change( weapon, this );
					weapon.pick_up( this, right_hand );
				}
			}
		}

		#region private
		// TODO(theGiallo): this needs to be saved if we have save/loads
		private static int next_uuid = 0;
		#endregion

		#region Unity
		private void Awake()
		{
			register( this );

			if ( controller == null )
			{
				controller = GetComponent<Character_Controller>();
			}
			if ( animator == null )
			{
				animator = GetComponentInChildren<Sprite_Animator>();
			}
		}
		private void Start()
		{
			if ( right_hand.childCount > 0 )
			{
				Weapon w = right_hand.GetChild( 0 ).GetComponent<Weapon>();
				if ( w != null )
				{
					status.weapon.change( w, this );
					w.pick_up( this, right_hand );
				}
			}

			status.normal_max_hp.change( normal_max_hp, null );
			status.max_hp.change( normal_max_hp, null );
			status.hp.change( normal_max_hp, null );
			status.dps.change( dps, null );
			status.has_life.change( has_life, null );
			status.is_alive.change( has_life, null );
			status.fov_deg.change( fov_deg, null );

			status.hp.on_will_change.Subscribe( ushort.MaxValue, ( data )=>{
				if ( data.new_value <= 0 )
				{
					data.new_value = 0;
				}
				return Chained.CONTINUE;
			} );
			status.hp.on_did_change.Subscribe( 0, ( data )=>{
				if ( data.new_value <= 0 && status.is_alive )
				{
					status.is_alive.change( false, data.entity_source );
				}
				return Chained.CONTINUE;
			} );
			status.hp.on_did_change.Subscribe( 0, ( data )=>{
				float delta = data.new_value - data.previous;
				if ( delta < 0 )
				{
					while ( delta < 0 )
					{
						int heart_halves;
						if ( delta <= -2 )
						{
							heart_halves = 2;
						} else
						{
							heart_halves = 1;
						}
						delta += 2;
						BP_Damage.spawn( transform.position + transform.up * height, heart_halves );
					}
				}
				return Chained.CONTINUE;
			} );

			status.is_alive.on_did_change.Subscribe( 0, ( data )=>{
				animator  .enabled = data.new_value;
				if ( data.new_value )
				{
					controller.constrain_rb();
				} else
				{
					controller.free_rb();
					if ( status.weapon.value != null )
					{
						status.weapon.value.drop();
						status.weapon.change( null, this );
					}
				}
				controller.enabled = data.new_value;
				return Chained.CONTINUE;
			} );
		}
		private void Update()
		{
		}
		private void OnDestroy()
		{
			unregister( this );
		}
		#endregion
	}
}
