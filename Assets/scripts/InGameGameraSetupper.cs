﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using tg;
using static tg.tg;
using UnityEngine.UI;

namespace tg
{
	public class InGameGameraSetupper : MonoBehaviour
	{
		public Camera   in_game_camera;
		public RawImage raw_image;

		#region Unity
		void
		Start()
		{
			Vector2 ref_res = new Vector2Int( 1920, 1080 );

			Resolution_Manager.register( new Resolution_Manager.Render_Camera_Target()
			{
				camera = in_game_camera,
				raw_image = raw_image,
				rt = null,
				res_scale = Vector2.one
			} );

			Resolution_Manager.set_max_resolution( new Vector2Int( 1920, 1080 ) );

		}
		#endregion Unity
	}
}
