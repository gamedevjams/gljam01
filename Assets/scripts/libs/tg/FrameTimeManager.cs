﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System;

namespace tg
{
	public class FrameTimeManager : MonoBehaviour
	{
		public static MonotonicTimestamp root_timestamp         { get; private set; }
		public static MonotonicTimestamp frame_border_timestamp { get; private set; }
		public static double             last_frame_span_ms     { get; private set; }
		public static double             frame_time_ms          { get; private set; } // NOTE(theGiallo): from root time
		public static double             curr_fixu_end_time_ms  { get; private set; } // NOTE(theGiallo): from root time
		public static double             delta_fixu_ms          { get; private set; }
		public static float root_fixed_time_sec         { get; private set; }
		public static float root_fixed_unscaled_time_sec{ get; private set; }

		public static bool first_fixed_time_update_of_frame { get; private set; }

		#region private
		private static bool passed_first_fixed_update;
		private static bool got_root_timestamp;
		private static FrameTimeManager instance;
		private int fixu_count = 0;
		#endregion

		#region Unity
		private void Awake()
		{
			if ( instance != null )
			{
				Destroy( gameObject );
				return;
			}
			MonotonicTimestamp now = MonotonicTimestamp.Now();
			#if false
			if ( ! got_root_timestamp )
			{
				got_root_timestamp = true;
				frame_border_timestamp = root_timestamp = now;
				curr_fixu_end_time_ms = 0;
			}
			#endif
			debug_assert( instance == null );
			instance = this;
			DontDestroyOnLoad( gameObject );
		}
		private void Start()
		{
		}
		private void FixedUpdate()
		{
			double last = curr_fixu_end_time_ms;
			if ( ! passed_first_fixed_update )
			{
				MonotonicTimestamp now = MonotonicTimestamp.Now();
				first_fixed_time_update_of_frame = true;

				if ( ! got_root_timestamp )
				{
					got_root_timestamp = true;
					frame_border_timestamp = root_timestamp = now;
					curr_fixu_end_time_ms = Time.fixedDeltaTime;
					root_fixed_time_sec = Time.fixedTime;
					root_fixed_unscaled_time_sec = Time.fixedUnscaledTime;
				}

				debug_assert( ( now - frame_border_timestamp ).TotalMilliseconds >= 0 );
				debug_assert( ( now - root_timestamp ).TotalMilliseconds >= 0 );
				last_frame_span_ms = ( now - frame_border_timestamp ).TotalMilliseconds;
				frame_time_ms      = ( now - root_timestamp         ).TotalMilliseconds;
				frame_border_timestamp = now;

				passed_first_fixed_update = true;
			} else
			{
				first_fixed_time_update_of_frame = false;
			}

			{
				#if false
				curr_fixu_end_time_ms += Time.unscaledDeltaTime * 1000.0;
				#elif false
				curr_fixu_end_time_ms += ( Time.fixedDeltaTime / Time.timeScale ) * 1000.0;
				#elif false
				curr_fixu_end_time_ms = ( Time.fixedTime - root_fixed_time_sec ) * 1000.0;
				#else
				curr_fixu_end_time_ms = ( Time.fixedUnscaledTime - root_fixed_unscaled_time_sec ) * 1000.0;
				#endif
			}

			delta_fixu_ms = curr_fixu_end_time_ms - last;
			++fixu_count;
		}
		private void Update()
		{
			//log( $"fixu {fixu_count}" );
			fixu_count = 0;
			first_fixed_time_update_of_frame = false;
			passed_first_fixed_update = false;
		}
		#endregion
	}

	public
	class
	Fixed_Timer
	{
		private double start_time_ms;
		private double last_time_updated_ms;
		public  float  elapsed_ms { get; private set; }
		public  float  duration_ms { get; private set; }
		public  float  remaining_ms => ended ? 0 : duration_ms - elapsed_ms;
		public  bool   ended      { get; private set; } = true;
		public  bool   just_ended { get; private set; }

		public Fixed_Timer( float duration_ms )
		{
			set_duration( duration_ms );
		}

		public
		void
		set_duration( float duration_ms )
		{
			this.duration_ms = duration_ms;
		}
		public
		void
		restart()
		{
			start_time_ms = FrameTimeManager.curr_fixu_end_time_ms;
			ended      = false;
			just_ended = false;
		}
		public
		void
		fixed_update()
		{
			just_ended = false;
			last_time_updated_ms = FrameTimeManager.curr_fixu_end_time_ms;
			elapsed_ms = (float)( last_time_updated_ms - start_time_ms );
			debug_assert( elapsed_ms >= 0 );
			if ( elapsed_ms >= duration_ms )
			{
				if ( ! ended )
				{
					just_ended = true;
				}
				ended = true;
			}
		}
	}
}
