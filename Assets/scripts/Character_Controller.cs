﻿#define TG_CHARACTER_CONTROLLER_STEP_CHECKER_ENABLED
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	public class Character_Controller : MonoBehaviour
	{
		public enum
		Action_Index
		{
			//WALK,
			//WALK_BACK,
			//WALK_SIDEWAYS_LEFT,
			//WALK_SIDEWAYS_RIGHT,
			USE_ALT,
			USE,
			INTERACT,
			JUMP,
			RUN,
			ROLL,
			// ---
			__COUNT,
		}
		public enum
		Action_On
		{
			FRONT,
			STATE,
		}
		//public Action_On[] actions_on = new Action_On[ACTIONS_COUNT];
		public const int ACTIONS_COUNT = (int)Action_Index.__COUNT;
		public class
		Controls_Commands
		{
			public Bit_Vector actions_on       = new Bit_Vector( ACTIONS_COUNT );
			public Bit_Vector actions_just_on  = new Bit_Vector( ACTIONS_COUNT );
			public Bit_Vector actions_just_off = new Bit_Vector( ACTIONS_COUNT );
			public float2     movement_v2;

			public bool walking()
			{
				bool ret = lengthsq( movement_v2 ) > 0;
				return ret;
			}
		}

		public static
		void
		set_in_game( bool in_game )
		{
			capturing_mouse = in_game;
			//update_lock_cursor();
		}

		public Entity entity;
		public bool lock_cursor;
		public bool control_camera;

		[Header("Structure")]
		public Camera camera_main;
		public RawImage camera_main_raw_image;
		public Animator animator;
		public Transform ground_zero;

		[Space]
		public bool spawn_debug_arrows = false;
		public GameObject collision_marker_prefab;

		[Space]
		public Transform step_check_area_foot;
		public Step_Checker step_checker;
		public float char_height;
		public float step_height_standing = 0.4f;
		public float step_height_crouched = 0.3f;
		public float curr_step_max_height => max_step();
		public float step_lenght = 0.35f;

		public
		void
		constrain_rb()
		{
			body_rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
		}
		public
		void
		free_rb()
		{
			body_rb.constraints = RigidbodyConstraints.None;
		}

		#region private

		private
		float
		max_step( )
		{
			float ret =
			   stance == Stance.STANDING
			   ? step_height_standing
			   : step_height_crouched;
			return ret;
		}

		private
		void
		update_step_check_area( Vector3 vel, float step_height )
		{
			//Vector3 p = step_check_area_foot.localPosition;
			//p.y = step_max_height;
			//step_check_area_foot.localPosition = p;

			Vector3 v = vel;// body_rb.velocity;
			Vector3 loc_v_dir = transform.InverseTransformVector( v ).normalized;
			//log( $"step_check_area_foot.position {step_check_area_foot.position}" );
			step_check_area_foot.localPosition = ground_zero.localPosition + loc_v_dir * step_lenght + new Vector3( 0, step_height, 0 );
			//log( $"vel {vel} loc_v_dir {loc_v_dir} step_check_area_foot.position {step_check_area_foot.position} ground_zero.position {ground_zero.position}" );
			//step_checker.box.center = step_checker.box.center;
		}


		#region controls
		public Controls_Commands controls_commands = new Controls_Commands();

		private
		bool
		action_just_on( Action_Index a )
		{
			bool ret = controls_commands.actions_just_on[(int)a] == 1;
			return ret;
		}
		private
		bool
		action_just_off( Action_Index a )
		{
			bool ret = controls_commands.actions_just_off[(int)a] == 1;
			return ret;
		}
		private
		bool
		action_on( Action_Index a )
		{
			bool ret = controls_commands.actions_on[(int)a] == 1;
			return ret;
		}
		//private
		//float
		//delta_camera_yaw()
		//{
		//	float ret = controls_commands.delta_yaw_deg;
		//	return ret;
		//}
		//private
		//float
		//delta_camera_pitch()
		//{
		//	float ret = controls_commands.delta_pitch_deg;
		//	return ret;
		//}

		#endregion controls

		public Transform body_tr;
		public Rigidbody body_rb;

		#region controller variables
		[Header("Movement")]
		[Tooltip("m/s")]
		public float normal_movement_speed = 4;
		[Tooltip("m/s")]
		public float run_movement_speed = 8;
		[Tooltip("m/s")]
		[Range(0.01f,50f)]
		public float min_walking_static_vel;
		[Tooltip("m/s")]
		[Range(0.01f,50f)]
		public float initial_movement_speed;
		public float drag_if_stopped;
		public float default_drag;
		public float angular_grag_if_not_walking = 0.05f;
		public float vel_dir_up_coefficient = 0.1f;
		public float anti_gravity_coefficient = 2f / 3f;
		public bool  anti_gravity_as_grounded = true;
		public bool  anti_gravity = true;
		public float grounded_height = 0.3f;
		public float grounded_forgiveness_ms = 500f;
		public float walking_back_vel_coeff = 0.2f;
		public float walking_sideways_vel_coeff = 0.4f;
		public bool  force_initial_velocity = false;
		public float max_rotation_deg_per_sec = 180f;
		public float stun_after_land_forgiven_vel = 2f;
		public float stun_after_land_ms_per_vel = 100f;

		[Space]
		[Range(0,2)]
		public float prev_walk_vel_attenuator = 0.9f;
		public bool  contrast_old_walk_vel = false;

		[Header("Friction")]
		public float max_friction_force = 4f;
		public float straight_friction_factor = 0.8f;
		public float max_straight_friction_force = 4f;

		[Header("Camera")]
		[Range(-180,180)]
		public float camera_pitch_min_deg;
		[Range(-180,180)]
		public float camera_pitch_max_deg;

		[Space]
		public bool body_yaw_manual = false;

		[Header("Feet")]
		public PhysicMaterial feet_physicMaterial;
		public float          static_friction_threshold_value = 10f;
		public float          slipping_velocity = 0.1f;

		[Header("Jump")]
		public float jump_force_m = 1000f;
		public float jump_timer_ms = 1000f;

		[Header("Stance")]
		[Foldout("Stance", true)]
		public float height_standup  =  1f;
		public float height_crouched =  0f;
		public float height_retraction_if_blocked = 0.005f;
		public float standing_to_crouch_timer_ms = 500;
		public float crouch_to_standing_timer_ms = 500;
		public float standing_movement_modifier = 1f;
		[Foldout("Stance", false )]
		public float crouch_movement_modifier = 0.3333f;

		public bool  is_grounded { get; private set; }
		public bool  as_grounded { get; private set; }
		public bool  jumped_and_not_jet_grounded { get; private set; }
		public float last_ms_grounded { get; private set; }
		private bool was_walking;
		private Vector3 prev_walk_vel;
		public Vector3 curr_walk_velocity { get; private set; }
		public Vector3 adjusted_walk_velocity { get; private set; }
		public Vector3 contrasting_vel { get; private set; }

		public float movement_speed { get; private set; }
		public bool  is_walking     { get; private set; }

		//public float camera_pitch_deg { get; private set; }
		//public float last_camera_pitch_deg { get; private set; }
		public float body_yaw_deg { get; private set; }
		public float abs_vel { get; private set; }  = 0;
		public float curr_vel_fw { get; private set; }
		public float movement_velocity { get; private set; }
		public Stance stance { get; private set; }
		public enum Stance
		{
			STANDING     = 0,
			CROUCHED = 1,
		}
		public float max_height { get; private set; } // NOTE(theGiallo): max recorded height of jump

		public bool   top_blocked { get; private set; } = false;
		public bool   head_top_blocked { get; private set; } = false;

		public Vector3 prev_body_rot { get; private set; }

		private Fixed_Timer crouch_to_standing_timer;
		private Fixed_Timer standing_to_crouch_timer;
		private Fixed_Timer landing_timer;
		private Fixed_Timer jump_timer;

		//private int animator_id_height;
		private int animator_id_xz_speed;
		private int animator_id_walk_speed;
		private int animator_id_stance;
		private int animator_id_grounded;
		private int animator_id_really_grounded;

		private
		struct
		Friction
		{
			public float dyn;
			public float stc;
			public PhysicMaterialCombine combine;
			public Friction( PhysicMaterial pm )
			{
				dyn     = pm.dynamicFriction;
				stc     = pm.staticFriction;
				combine = pm.frictionCombine;
			}
			public
			void
			sum( PhysicMaterial colliding, PhysicMaterial to_avg )
			{
				// NOTE(theGiallo):  The priority order is as follows: Average < Minimum < Multiply < Maximum.
				// https://docs.unity3d.com/ScriptReference/PhysicMaterialCombine.html

				if ( colliding.frictionCombine == PhysicMaterialCombine.Maximum || to_avg.frictionCombine == PhysicMaterialCombine.Maximum )
				{
					dyn += Mathf.Max( colliding.dynamicFriction, to_avg.dynamicFriction );
					stc += Mathf.Max( colliding.staticFriction , to_avg.staticFriction  );
				} else
				if ( colliding.frictionCombine == PhysicMaterialCombine.Multiply || to_avg.frictionCombine == PhysicMaterialCombine.Multiply )
				{
					dyn += colliding.dynamicFriction * to_avg.dynamicFriction;
					stc += colliding.staticFriction  * to_avg.staticFriction;
				} else
				if ( colliding.frictionCombine == PhysicMaterialCombine.Minimum || to_avg.frictionCombine == PhysicMaterialCombine.Minimum )
				{
					dyn += Mathf.Min( colliding.dynamicFriction, to_avg.dynamicFriction );
					stc += Mathf.Min( colliding.staticFriction , to_avg.staticFriction  );
				} else
				if ( colliding.frictionCombine == PhysicMaterialCombine.Average || to_avg.frictionCombine == PhysicMaterialCombine.Average )
				{
					dyn += ( colliding.dynamicFriction + to_avg.dynamicFriction ) * 0.5f;
					stc += ( colliding.staticFriction  + to_avg.staticFriction  ) * 0.5f;
				}
			}
		}

		#endregion

		private Vector3 checkpoint_position = new Vector3( 32, 9, -15 );
		private Quaternion checkpoint_rotation = Quaternion.identity;
		private
		void
		fixed_update_controls()
		{
			if ( false )
			{
				Vector3 r = transform.localRotation.eulerAngles;
				r.x = r.z = 0;
				transform.localRotation = Quaternion.Euler( r );
			}

			if ( ! capturing_mouse )
			{
				return;
			}

			bool anti_gravity_now = true;

			if ( Keyboard.current[Key.Backspace].wasPressedThisFrame )
			{
				checkpoint_position = transform.position;
				checkpoint_rotation = transform.rotation;
				log( $"save position and rotation {checkpoint_position} {checkpoint_rotation}" );
			}
			if ( Keyboard.current[Key.F5].wasPressedThisFrame )
			{
				transform.position = checkpoint_position;
				transform.rotation = checkpoint_rotation;
				body_rb.velocity = Vector3.zero;
			}
			if ( Keyboard.current[Key.Enter].wasPressedThisFrame )
			{
				transform.position = Vector3.zero;
				body_rb.velocity = Vector3.zero;
			}

			// NOTE(theGiallo): check for blocking collisions ontop
			top_blocked = false;
			head_top_blocked = false;
			{
				float top_h = step_checker.box.size.y * step_checker.box.transform.transform.lossyScale.y / body_tr.lossyScale.y;
				float eps_h = 0.1f;
				for ( int i = 0; i != colliding_points.Count; ++i )
				{
					Colliding_Point cp = colliding_points[i];
					if ( ! cp.has_grounded_local_pos )
					{
						continue;
					}
					if ( cp.grounded_local_pos.y >= top_h - eps_h )
					{
						top_blocked = true;
						// TODO(theGiallo): check this
						//if ( cp.contact_point.thisCollider.transform.parent == head_colliders_container )
						{
							head_top_blocked = true;
						}
					}
				}
			}

			//if ( action_on( Action_Index.CROUCH ) )
			//{
			//	stance = Stance.CROUCHED;
			//} else
			//{
			//	stance = Stance.STANDING;
			//}


			if ( ! landing_timer.ended )
			{
				landing_timer.fixed_update();
			}
			if ( ! jump_timer.ended )
			{
				jump_timer.fixed_update();
			}

			float curr_ms = (float) FrameTimeManager.curr_fixu_end_time_ms;
			bool new_as_grounded = is_grounded || curr_ms - last_ms_grounded < grounded_forgiveness_ms;
			bool landed = new_as_grounded && ! as_grounded;
			if ( landed )
			{
				log( $"max_height = {max_height}" );
				float vel = Mathf.Abs( body_rb.velocity.y );//.magnitude;
				log( $"vel = {vel}" );
				float ms_in_air = curr_ms - last_ms_grounded;
				if ( landing_timer.ended )
				{
					landing_timer.set_duration( stun_after_land_ms( vel ) );
					landing_timer.restart();
				} else
				{
					float ms = stun_after_land_ms( vel );
					ms += landing_timer.remaining_ms;
					landing_timer.set_duration( ms );
					landing_timer.restart();
				}
			}
			as_grounded = new_as_grounded;
			if ( is_grounded )
			{
				last_ms_grounded = curr_ms;
				jumped_and_not_jet_grounded = false;
			}

			float delta_body_yaw_deg     = 0;//delta_camera_yaw();
			bool movement_command_walking = controls_commands.walking();
			if ( movement_command_walking )
			{
				delta_body_yaw_deg = Vector2.SignedAngle( controls_commands.movement_v2, float2( body_tr.forward.x, body_tr.forward.z ) );
			}
			//float delta_camera_pitch_deg = 0;//delta_camera_pitch();
			float yaw_max_deg_per_fixu = max_rotation_deg_per_sec * Time.fixedDeltaTime;
			delta_body_yaw_deg = clamp( delta_body_yaw_deg, -yaw_max_deg_per_fixu, yaw_max_deg_per_fixu );
			//last_camera_pitch_deg = camera_pitch_deg;
			//camera_pitch_deg += delta_camera_pitch_deg;
			//camera_pitch_deg = clamp( camera_pitch_deg, camera_pitch_min_deg, camera_pitch_max_deg );
			float y_deg = body_rb.rotation.eulerAngles.y;
			Quaternion yaw = Quaternion.AngleAxis( y_deg, float3(0,1,0) );
			//Quaternion pitch = Quaternion.AngleAxis( camera_pitch_deg, float3(1,0,0) );
			if ( control_camera ) camera_main.transform.rotation = yaw;// * pitch;

			constrain_rb();
			body_rb.maxAngularVelocity = Mathf.PI * 2f / 0.1f;
			body_rb.maxDepenetrationVelocity = 0.1f * 120f;

			{
				Vector3 av = body_rb.angularVelocity;
				av = body_tr.InverseTransformVector( av );
				av.y = 0;
				av = body_tr.TransformVector( av );
				body_rb.angularVelocity = av;
			}
			if ( body_yaw_manual )
			{
				body_tr.Rotate( Vector3.up, delta_body_yaw_deg, Space.Self );
			} else
			{
				body_rb.angularDrag = 0;
				Vector3 dav = new Vector3( 0, Mathf.Deg2Rad * delta_body_yaw_deg / Time.fixedDeltaTime, 0 );
				#if false
				// NOTE(theGiallo): this causes the object to be subject to less gravity, somehow
				body_rb.angularVelocity += body_tr.TransformVector( dav );
				#else
				body_rb.AddTorque( dav, ForceMode.VelocityChange );
				#endif
			}

			body_rb.drag = default_drag;

			bool is_walking_fwd        = false;
			bool is_walking_back       = false;
			bool is_walking_side_left  = false;
			bool is_walking_side_right = false;
			bool is_walking_straight   = false;
			bool is_walking_sideways   = false;
			is_walking            = false;
			bool can_jump              = as_grounded && ! jumped_and_not_jet_grounded && stance == Stance.STANDING && jump_timer.ended;
			bool jumped                = false;
			bool stepped               = false;

			if ( as_grounded )
			{
				is_walking_fwd        = movement_command_walking;//action_on( Action_Index.WALK );
				is_walking_back       = false;//action_on( Action_Index.WALK_BACK );
				is_walking_side_left  = false;//action_on( Action_Index.WALK_SIDEWAYS_LEFT  );
				is_walking_side_right = false;//action_on( Action_Index.WALK_SIDEWAYS_RIGHT );
				is_walking_straight   = is_walking_fwd != is_walking_back;
				is_walking_sideways   = ! is_walking_straight && ( is_walking_side_left != is_walking_side_right );
				is_walking            = is_walking_straight || is_walking_sideways;

				if ( ! landing_timer.ended )
				{
					is_walking_fwd        = false;
					is_walking_back       = false;
					is_walking_side_left  = false;
					is_walking_side_right = false;
					is_walking_straight   = false;
					is_walking_sideways   = false;
					is_walking            = false;
					can_jump              = false;
				}
			}

			if ( action_just_on( Action_Index.JUMP ) )
			{
				// NOTE(theGiallo): jump
				if ( can_jump )
				{
					jumped = true;
					jumped_and_not_jet_grounded = true;
					can_jump = false;
					Vector3 jump_force = Vector3.up * jump_force_m;
					body_rb.AddForce( jump_force * ( ( 2f / 3f ) / Time.fixedDeltaTime ) );
					max_height = 0;
					log( $"jump {jump_force.y}" );
					jump_timer.restart();
				}
			}

			abs_vel = 0;
			if ( as_grounded )
			{
				Vector3 walk_dir = is_walking_sideways ? body_tr.right : body_tr.forward;
				float mov = 0f;
				if ( is_walking_straight && is_walking_fwd )
				{
					mov = 1f;
				} else
				if ( is_walking_straight && is_walking_back )
				{
					mov = walking_back_vel_coeff;
					walk_dir = - walk_dir;
				} else
				if ( is_walking_sideways && is_walking_side_left )
				{
					mov = walking_sideways_vel_coeff;
					walk_dir = - walk_dir;
				} else
				if ( is_walking_sideways && is_walking_side_right )
				{
					mov = walking_sideways_vel_coeff;
				}
				adjusted_walk_velocity = Vector3.zero;

				curr_walk_velocity = Vector3.zero;

				mov *= stance_movement_modifier();

				if ( is_walking )
				{
					movement_speed = action_on( Action_Index.RUN ) ? run_movement_speed : normal_movement_speed;
					abs_vel = movement_speed * mov;
					curr_walk_velocity = walk_dir * abs_vel;
					body_rb.drag = 0;
				}
				adjusted_walk_velocity += curr_walk_velocity;

				if ( was_walking && contrast_old_walk_vel )
				{
					Vector3 wn = adjusted_walk_velocity.normalized;
					Vector3 up = Vector3.Cross( wn, prev_walk_vel );
					Vector3 side = Vector3.Cross( up, wn );
					contrasting_vel = -side * prev_walk_vel_attenuator;
					adjusted_walk_velocity += contrasting_vel;
				}

				// NOTE(theGiallo): auto-stepping
				if ( can_jump )
				{
					bool has_to_step = false;
					bool can_step = true;
					float step_height = 0;
					float max_step_h = curr_step_max_height;
					for ( int i = 0; i != colliding_points.Count; ++i )
					{
						Colliding_Point cp = colliding_points[i];
						if ( cp.has_our_normal )
						{
							float cos_coll = Vector3.Dot( cp.our_normal, walk_dir );
							float cos_hpi = 0;
							bool is_obstacle = cos_coll > cos_hpi;
							debug_assert( cp.has_grounded_local_pos );
							float curr_step_height = cp.grounded_local_pos.y;
							bool this_can_step = curr_step_height <= max_step_h;
							bool this_has_to_step = is_obstacle && this_can_step;
							has_to_step = has_to_step || this_has_to_step;
							can_step    = can_step    || this_can_step;
							if ( this_has_to_step )
							{
								step_height = Mathf.Clamp( step_height, curr_step_height, max_step_h );
							}
						}
					}
					#if TG_CHARACTER_CONTROLLER_STEP_CHECKER_ENABLED
					update_step_check_area( curr_walk_velocity, step_height );
					if ( is_walking && step_checker.check_now() )
					{
						if ( has_to_step && step_height < 0.05f )
						{
							//log( $"step_height {step_height}" );
							//transform.position += new Vector3( 0, step_height, 0 );
						} else
						if ( has_to_step && can_step )
						{
							float g = Physics.gravity.y;
							// step_height = 0.5f * g * t*t + ( v0 + v ) * t;
							//float step_time_per_1m = 1f / movement_speed;
							//float t = step_time_per_1m * step_height;//Time.fixedDeltaTime;
							//if ( t > 1e-3 )
							{
								float v0 = body_rb.velocity.y;
								// {
								//    step_height = 0.5f * g * t*t + ( v0 + sv ) * t
								//    sv = ( step_height - 0.5f * g * t*t ) / t - v0;
								//    sv + v0 = ( step_height - 0.5f * g * t*t ) / t;
								//
								//    sv = g * t - v0;
								//    sv + v0 = - g * t;
								//    t = ( sv + v0 ) / -g
								// }
								// sv + v0 = ( step_height - 0.5f * g * ( sv + v0 ) / -g * ( sv + v0 ) / -g ) / ( ( sv + v0 ) / -g );
								// sv + v0 = ( step_height - 0.5f * ( sv + v0 ) * ( sv + v0 ) / g ) * -g / ( sv + v0 );
								// sv + v0 = ( step_height - 0.5f / g * ( sv + v0 )^2 ) * -g / ( sv + v0 );
								// sv + v0 = ( step_height * g - 0.5f * ( sv + v0 )^2 ) / ( sv + v0 );
								// ( sv + v0 )^2 = - step_height * g + 0.5f * ( sv + v0 )^2;
								// 1.5 * ( sv + v0 )^2 = - step_height * g;
								// 1.5 * ( sv + v0 )^2 + step_height * g = 0;
								// sv * sv + 2 * v0 * sv + v0 * v0 - 2 / 3 * step_height * g = 0;
								#if true
								float V01 = Mathf.Sqrt( - step_height * g * 2f / 3f );
								float V = Mathf.Abs( V01 );
								float step_vel0 = V - v0;
								#else
								float b = 2 * v0;
								float hb = v0;
								float c = v0 * v0 - 2f / 3f * step_height * g;
								float sv0 = -hb + Mathf.Sqrt( hb * hb - c );
								float sv1 = -hb - Mathf.Sqrt( hb * hb - c );
								float step_vel0 = Mathf.Max( sv0, sv1 );
								#endif
								//float step_vel0 = ( step_height - 0.5f * g * t*t ) / t - v0;
								if ( step_vel0 > 0 )
								{
									float step_force_abs = step_vel0 / Time.fixedDeltaTime * 2f / 3f;
									Vector3 step_force = new Vector3( 0, step_force_abs, 0 );
									//body_rb.AddForce( step_force );
									body_rb.velocity = body_rb.velocity + new Vector3( 0, step_vel0, 0 );
									stepped = true;
									log( $"stepping {step_force} step_height: {step_height} v0: {v0} stepv0: {step_vel0} V: {V}" );
								} else
								{
									log( $"NOT stepping step_height: {step_height} v0: {v0} stepv0: {step_vel0} V: {V}" );
								}
							}// else
							{
								//log( $"not stepping t = {t}" );
							}
						}
					}
					#endif
				}

				if ( force_initial_velocity && ! was_walking && is_walking && body_rb.velocity.sqrMagnitude < 1 )
				{
					body_rb.velocity += walk_dir * abs_vel;

					//abs_vel = Mathf.Max( abs_vel, min_walking_static_vel );
				}
				curr_vel_fw = Mathf.Max( 0f, Vector3.Dot( body_rb.velocity, walk_dir ) );
				movement_velocity = 0;
				if ( is_walking )
				{
					movement_velocity = Mathf.Max( 0f, abs_vel - curr_vel_fw );
				}
				Vector3 vel_dir = walk_dir;//( walk_dir + body_node.up * vel_dir_up_coefficient ).normalized;
				Vector3 movement_force = vel_dir * ( movement_velocity / Time.fixedDeltaTime );
				//body_rigidbody.velocity += walk_velocity;

				// NOTE(theGiallo): friction like force
				if ( is_grounded )
				{
					Friction friction = default;
					int ground_count = 0;
					for ( int i = 0; i != colliding_points.Count; ++i )
					{
						Colliding_Point cp = colliding_points[i];
						if ( ! cp.has_grounded_local_pos )
						{
							continue;
						}

						PhysicMaterial pm = cp.contact_point.otherCollider.material;
						friction.sum( feet_physicMaterial, pm );
						++ground_count;
					}
					friction.dyn /= (float)ground_count;
					friction.stc /= (float)ground_count;

						Vector3 rbv = body_rb.velocity;
						float rbv_m = body_rb.velocity.magnitude;
						Vector3 desired_v = rbv + movement_velocity * vel_dir;
						float desired_v_m = desired_v.magnitude;


					bool is_slipping =
					     static_friction_threshold( friction.stc, movement_velocity )
					  || static_friction_threshold( friction.stc, rbv_m )
					  || static_friction_threshold( friction.stc, desired_v_m );

					if ( is_slipping )
					{
						anti_gravity_now = false;
					}

					float fr = is_slipping ? friction.dyn : friction.stc;
					//log( $"fr = {fr} dyn = {friction.dyn} stc = {friction.stc}" );

					if ( is_walking )//&& is_grounded )
					{
						float ok_v_m = Vector3.Dot( walk_dir, rbv );
						ok_v_m = Mathf.Max( 0, ok_v_m );
						Vector3 ok_v = walk_dir * ok_v_m;

						Vector3 bad_v = rbv - ok_v;
						float bad_v_m = bad_v.magnitude;
						float friction_m = Mathf.Min( max_friction_force, bad_v_m );
						bad_v = bad_v_m == 0f ? Vector3.zero : bad_v * ( friction_m / bad_v_m );

						body_rb.AddForce( ( - fr * 2f / 3f / Time.fixedDeltaTime ) * bad_v );

						if ( ok_v_m > abs_vel )
						{
							float straight_friction = ( ok_v_m - abs_vel ) * straight_friction_factor;
							straight_friction = Mathf.Min( max_straight_friction_force, straight_friction );

							body_rb.AddForce( -walk_dir * straight_friction / Time.fixedDeltaTime * fr );
						}

						if ( is_slipping )
						{
							movement_velocity = slipping_velocity;
							movement_force = movement_force.normalized * movement_velocity / Time.fixedDeltaTime;
						}
					} else
					//if ( is_grounded )
					{
						Vector3 stopping_friction_vel;
						stopping_friction_vel = -rbv * fr;
						body_rb.AddForce( stopping_friction_vel / Time.fixedDeltaTime );
						//log( "stopping force" );
					}
				}

				if ( is_walking && as_grounded )
				{
					body_rb.AddForce( movement_force );
				}

				was_walking = is_walking;
				prev_walk_vel = curr_walk_velocity;
			}

			// NOTE(theGiallo): anti-gravity
			bool anti_gravity_grounded = anti_gravity_as_grounded ? as_grounded : is_grounded;
			if ( anti_gravity_now && anti_gravity && ( ( anti_gravity_grounded && is_walking ) || jumped ) )
			{
				body_rb.AddForce( - Physics.gravity * ( anti_gravity_coefficient / Time.fixedDeltaTime ) );
			}

			float xz_speed;
			{
				Vector3 rbv = body_rb.velocity;
				float ok_v_m = Vector3.Dot( body_tr.forward, rbv );
				Vector3 v = body_tr.forward * ok_v_m;
				Vector2 vxz = new Vector2( v.x, v.z );
				xz_speed = vxz.magnitude;
			}
			animator.SetBool( animator_id_grounded, jumped ? false : as_grounded );
			animator.SetBool( animator_id_really_grounded, jumped ? false : is_grounded );
			//animator.SetFloat( animator_id_height, height );
			animator.SetInteger( animator_id_stance, (int)stance );
			animator.SetFloat( animator_id_walk_speed, abs_vel );
			animator.SetFloat( animator_id_xz_speed, xz_speed  );

			max_height = Mathf.Max( transform.localPosition.y, max_height );

			// NOTE(theGiallo): this will be updated by the OnCollision* that are called after the FixedUpdate
			is_grounded = false;
			colliding_points.Clear();
			//update_step_check_area( curr_walk_velocity );
		}

		private
		bool
		static_friction_threshold( float static_friction, float abs_vel )
		{
			bool ret = abs_vel / static_friction > static_friction_threshold_value;
			return ret;
		}

		private
		float
		stance_movement_modifier()
		{
			float ret = stance == Stance.STANDING ? standing_movement_modifier : crouch_movement_modifier;
			return ret;
		}

		private
		float
		stun_after_land_ms( float vel )
		{
			float ret = Mathf.Max( 0, vel - stun_after_land_forgiven_vel ) * stun_after_land_ms_per_vel;
			ret = Mathf.Min( ret, 3000f );
			return ret;
		}

		private
		struct
		Colliding_Point
		{
			public Vector3        our_normal;
			public bool           has_our_normal;
			public Vector3        grounded_local_pos;
			public bool           has_grounded_local_pos;
			public ContactPoint   contact_point;
		}
		private ContactPoint[] collision_contact_points = new ContactPoint[64];
		private List<Colliding_Point> colliding_points = new List<Colliding_Point>();
		private
		void
		check_collision_for_grounding( Collision collision )
		{
			float gh = grounded_height + ground_zero.localPosition.y;
			int ccount = collision.contactCount;
			if ( ccount > collision_contact_points.Length )
			{
				collision_contact_points = new ContactPoint[ccount];
			}
			collision.GetContacts( collision_contact_points );
			for ( int i = 0; i != ccount; ++i )
			{
				ContactPoint cp = collision_contact_points[i];

				//if ( cp.otherCollider.transform.IsChildOf( transform ) )
				//{
				//	continue;
				//}

				Colliding_Point colliding_point = new Colliding_Point();
				colliding_point.contact_point = cp;
				//log( $"{cp.normal} | {cp.normal.magnitude}" );
				Vector3 lp = transform.InverseTransformPoint( cp.point );
				colliding_point.grounded_local_pos = lp;
				colliding_point.has_grounded_local_pos = true;
				if ( cp.normal.y > 0 )
				{
					if ( lp.y < gh )
					{
						is_grounded = true;
					}
				}

				{
					Vector3 dir_towards_us = cp.normal;
					float abs_dp = Mathf.Max( 1e-3f, 16f * Mathf.Abs( cp.separation ) );
					Vector3 dp = -dir_towards_us * abs_dp;
					Vector3 p = cp.point + dp;
					Ray ray = new Ray( p, dir_towards_us );
					float max_d = Mathf.Max( 0.1f, abs_dp * 3f );
					bool did_hit = cp.thisCollider.Raycast( ray, out RaycastHit hit, max_d );
					if ( did_hit )
					{
						colliding_point.our_normal = hit.normal;
						colliding_point.has_our_normal = true;
					} else
					{
						//log( $"abs_dp = {abs_dp} dirtus = ({dir_towards_us.x}, {dir_towards_us.y}, {dir_towards_us.z}) dp = ({dp.x}, {dp.y}, {dp.z}) p = ({p.x}, {p.y}, {p.z}) cp.separation = {cp.separation}" );
						//log( $"pos: {transform.position.x}, {transform.position.y}, {transform.position.z}" );
						//log_err( "collision normal ray didn't hit" );
						//ILLEGAL_PATH( "collision normal ray didn't hit" );
					}
				}

				colliding_points.Add( colliding_point );

				if ( spawn_debug_arrows )
				{
					GameObject go = Instantiate( collision_marker_prefab, null, false );
					Quaternion r = Quaternion.FromToRotation( Vector3.up, cp.normal );
					go.transform.SetPositionAndRotation( cp.point, r );
				}
				//log( cp.point.ToString() );
			}
		}
		private void OnCollisionEnter( Collision collision )
		{
			check_collision_for_grounding( collision );
		}
		private void OnCollisionStay( Collision collision )
		{
			check_collision_for_grounding( collision );
		}
		private void OnCollisionExit( Collision collision )
		{
		}

		private static bool capturing_mouse = true;

		private void update_lock_cursor()
		{
			if ( lock_cursor ) do_update_lock_cursor();
		}
		private static void do_update_lock_cursor()
		{
			Cursor.lockState = capturing_mouse ? CursorLockMode.Locked : CursorLockMode.None;
		}
		#endregion

		#region Unity
		private void Awake()
		{
			//actions_on[(int)Action_Index.WALK               ] = Action_On.STATE;
			//actions_on[(int)Action_Index.WALK_BACK          ] = Action_On.STATE;
			//actions_on[(int)Action_Index.WALK_SIDEWAYS_LEFT ] = Action_On.STATE;
			//actions_on[(int)Action_Index.WALK_SIDEWAYS_RIGHT] = Action_On.STATE;
			//actions_on[(int)Action_Index.AIM                ] = Action_On.STATE;
			//actions_on[(int)Action_Index.USE                ] = Action_On.STATE;
			//actions_on[(int)Action_Index.INTERACT           ] = Action_On.STATE;
			//actions_on[(int)Action_Index.JUMP               ] = Action_On.FRONT;
			//actions_on[(int)Action_Index.RUN                ] = Action_On.STATE;
			//actions_on[(int)Action_Index.CROUCH             ] = Action_On.STATE;

			//animator_id_height     = Animator.StringToHash( "height"     );
			animator_id_xz_speed   = Animator.StringToHash( "xz_speed"   );
			animator_id_walk_speed = Animator.StringToHash( "walk_speed" );
			animator_id_stance     = Animator.StringToHash( "stance" );
			animator_id_grounded   = Animator.StringToHash( "grounded"   );
			animator_id_really_grounded = Animator.StringToHash( "really_grounded"   );
		}
		private void Start()
		{
			#if ! TG_CHARACTER_CONTROLLER_STEP_CHECKER_ENABLED
			step_checker.gameObject.SetActive( false );
			#endif

			movement_speed = normal_movement_speed;

			update_lock_cursor();
			if ( control_camera )
			if ( camera_main_raw_image != null )
			{
				Vector2 ref_res = new Vector2Int( 1920, 1080 );

				Resolution_Manager.register( new Resolution_Manager.Render_Camera_Target(){
					camera = camera_main,
					raw_image = camera_main_raw_image,
					rt = null,
					res_scale = Vector2.one
				} );

				Resolution_Manager.set_max_resolution( new Vector2Int( 1920, 1080 ) );
			}

			//update_step_check_area();

			prev_body_rot = body_tr.localRotation.eulerAngles;

			crouch_to_standing_timer = new Fixed_Timer( crouch_to_standing_timer_ms );
			standing_to_crouch_timer = new Fixed_Timer( standing_to_crouch_timer_ms );
			landing_timer            = new Fixed_Timer( 0 );
			jump_timer               = new Fixed_Timer( jump_timer_ms );
		}
		private void Update()
		{
		}
		private void FixedUpdate()
		{
			//log( $"FixedUpdate {FrameTimeManager.curr_fixu_end_time_ms}" );
			fixed_update_controls();

			if ( capturing_mouse )
			{
				if ( action_just_on( Action_Index.USE ) )
				{
					entity.use();
				} else
				if ( action_just_on( Action_Index.USE_ALT ) )
				{
					entity.use_alt();
				} else
				if ( action_just_on( Action_Index.INTERACT ) )
				{
					entity.interact();
				}
			}
		}
		#endregion
	}
}
