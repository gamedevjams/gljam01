﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using UnityEngine.SceneManagement;

namespace tg
{
	public class Paused_Menu : MonoBehaviour
	{
		public Button unpause_button;
		public Button mappings_button;
		public Button restart_button;
		public Button exit_button;
		public GameObject mappings_menu;

		public string main_menu_scene_name;

		#region private
		private
		void
		unpause()
		{
			In_Game_Menu_Handler.exit_menu();
		}
		private
		void
		mappings()
		{
			mappings_menu.SetActive( true );
			gameObject.SetActive( false );
		}
		private
		void
		restart()
		{
			In_Game_Menu_Handler.exit_menu();
			Loading.reload();
		}
		private
		void
		exit()
		{
			In_Game_Menu_Handler.unpause();
			Loading.load( main_menu_scene_name );
		}
		#endregion

		#region Unity
		private void Awake()
		{
			unpause_button .onClick.AddListener( unpause );
			mappings_button.onClick.AddListener( mappings );
			restart_button .onClick.AddListener( restart );
			exit_button    .onClick.AddListener( exit );
		}
		private void Start()
		{
			mappings_menu.SetActive( false );
			gameObject.SetActive( true );
		}
		private void Update()
		{
		}
		#endregion
	}
}
