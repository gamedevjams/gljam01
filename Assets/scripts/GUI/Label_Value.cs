﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	public class Label_Value : MonoBehaviour
	{
		public TMP_Text label;
		public TMP_Text value;
		public string    label_str;
		public string    value_str;

		public
		void
		set_label( string label_str)
		{
			label.text = this.label_str = label_str;
		}

		public
		void
		set_value( string value_str )
		{
			value.text = this.value_str = value_str;
		}

		#region private
		#endregion

		#region Unity
		#if UNITY_EDITOR
		private void OnValidate()
		{
			set_label( label_str );
			set_value( value_str );
		}
		#endif
		private void Awake()
		{
		}
		private void Start()
		{
		}
		private void Update()
		{
		}
		#endregion
	}
}
