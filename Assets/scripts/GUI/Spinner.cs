﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	public class Spinner : MonoBehaviour
	{
		public float deg_per_sec = 30f;

		#region private
		private RectTransform rect_transform;
		#endregion

		#region Unity
		private void Awake()
		{
			rect_transform = GetComponent<RectTransform>();
		}
		private void Start()
		{
		}
		private void Update()
		{
			rect_transform.Rotate( 0, 0, Time.deltaTime * deg_per_sec );
		}
		#endregion
	}
}
