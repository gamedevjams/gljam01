﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	public class Character_Controller_Debug_GUI : MonoBehaviour
	{
		public Character_Controller character_controller;

		public Label_Value label_rb_abs_vel;
		public Label_Value label_walk_vel;
		public Label_Value label_is_grounded;
		public Label_Value label_as_grounded;
		public Label_Value label_jumped_not_jet_landed;

		#region private
		#endregion

		#region Unity
		private void Awake()
		{
		}
		private void Start()
		{
		}
		private void Update()
		{
			label_rb_abs_vel.set_value( character_controller.body_rb.velocity.magnitude.ToString( "000.000" ) );
			label_walk_vel.set_value( character_controller.abs_vel.ToString( "000.000" ) );
			label_is_grounded.set_value( character_controller.is_grounded.ToString() );
			label_as_grounded.set_value( character_controller.as_grounded.ToString() );
			label_jumped_not_jet_landed.set_value( character_controller.jumped_and_not_jet_grounded.ToString() );
		}
		#endregion
	}
}
