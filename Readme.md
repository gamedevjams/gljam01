# gljam01 entry - unnamed

## Code rules

1. Indent with tabs, align with spaces
1. Snake case, variables and functions all lowercase, Types with Upper case initials
1. In Visual Studio install these plugins:
    - https://marketplace.visualstudio.com/items?itemName=JakubBielawa.LineEndingsUnifier
        set it up to use Unix line endings and to unify them at save
    - https://marketplace.visualstudio.com/items?itemName=PawelKadluczka-MSFT.TrailingSpaceFlagger
1. Setup your gitconfig like this: (you can find it in your user folder)

        [user]
            email = your@email.com
            name = Your Name
        [alias]
            lga = log --all --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --
            lg = log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --

        [merge]
            tool = WinMerge
        [diff]
            tool = WinMerge

        [difftool "WinMerge"]
            cmd = \"C:\\Program Files\\WinMerge\\WinMergeU.exe\" -e -u -dl \"Old $BASE\" -dr \"New $BASE\" \"$LOCAL\" \"$REMOTE\"
            trustExitCode = true

        [mergetool "WinMerge"]
            cmd = \"C:\\Program Files\\WinMerge\\WinMergeU.exe\" -e -u -dl \"Local $LOCAL\" -dm \"Base $BASE\" -dr \"Remote $REMOTE\" \"$LOCAL\" \"$BASE\" \"$REMOTE\" -o \"$MERGED\"
            trustExitCode = true
            keepBackup = false
        [filter "lfs"]
            clean = git-lfs clean -- %f
            smudge = git-lfs smudge -- %f
            process = git-lfs filter-process
            required = true

1. To log use tg.log* wrappers
1. To format strings use the string interpolator as in `$"string {variable}"`
1. No egyptian curly braces please
